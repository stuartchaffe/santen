# Origin

Origin is a boilerplate for web projects which includes a collection of SASS files and a [gulp.js](http://gulpjs.com) config file.

## Installation

First, you'll need to install [gulp.js](http://gulpjs.com/).

You can then install via the command line:

	$ git clone git@github.com:marcjenkins/origin.git your-project
	$ cd your-project
	$ sudo npm install

Start Browser Sync using Gulp: `gulp watch`.

## Credit & inspiration

* [normalize.css](http://necolas.github.io/normalize.css/)
* [inuitcss](http://inuitcss.com)
* [itcss](http://itcss.io)
* [SMACSS](https://smacss.com)
* [HTML5 Boilerplate](https://html5boilerplate.com/)
* [Bootstrap](https://github.com/twbs/bootstrap)
